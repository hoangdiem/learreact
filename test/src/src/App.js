import React from 'react';
import './App.css';
import Selection from './Components/Selection';
import PersonInfo from './Components/PersonInfo';


class App extends React.Component{
  state = {
    people: [],
    idSelected: 1
  }

  handleChange = (e) => {
    const value = e.target.value;
    this.setState({
        idSelected: value
    })
  }

  render() {
    return (
      <div className="App">
        <h1>HOOKS REACT</h1>
        <div></div>
        <div className="content">
          <Selection handleChanged={this.handleChange} idSelected={this.state.idSelected}/>
          <PersonInfo person={this.state.idSelected}/>
        </div>
      </div>
    );
  }
}

export default App;
