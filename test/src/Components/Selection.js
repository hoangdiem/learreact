import React from 'react';
import Axios from 'axios';

export default class Selection extends React.Component {
  state = {
    people: []
  };

  componentDidMount() {
    Axios.get('https://swapi.co/api/people/').then(res => {
      const people = res.data.results;
      const listName = [];
      people.map((item, index) =>
        listName.push({
          name: item.name,
          id: index + 1
        })
      );

      this.setState({
        people: listName
      });
    });
  }

  handleChange = e => {
    this.props.handleChanged(e);
  };

  render() {
    return (
      <select value={this.props.idSelected} onChange={this.handleChange}>
        {this.state.people.map(item => (
          <option value={item.id} key={item.id}>
            {item.name}
          </option>
        ))}
      </select>
    );
  }
}
