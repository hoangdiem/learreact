import React from 'react';
import Axios from 'axios';

export default class PersonInfo extends React.Component {
  constructor(props) {
    super(props);
    this.state = { user: {}, loading: false };
  }

  shouldComponentUpdate(nextProps, nextState) {
    return this.props.person !== nextProps.person;
  }

  componentDidUpdate(prevProps) {
    prevProps.person !== this.props.person && this.getData();
  }

  componentDidMount() {
    this.getData();
  }

  getData() {
    this.setState({ loading: true });
    Axios.get('https://swapi.co/api/people/' + this.props.person)
      .then(res => {
        this.setState({
          user: res.data,
          loading: false
        });
      })
      .catch(err => {
        console.log(err);
      });
  }

  render() {
    const content =
      !this.state.loading && !this.state.user.name ? (
        <div>loading...</div>
      ) : (
        <div className='person'>
          <h2 className='person__name'>{this.state.user.name}</h2>

          <dl>
            <p>
              <dt>gender</dt>
              <dd>{this.state.user.gender}</dd>
            </p>
            <p>
              <dt>Hair color</dt>
              <dd>{this.state.user.hair_color}</dd>
            </p>
            <p>
              <dt>Skin color</dt>
              <dd>{this.state.user.skin_color}</dd>
            </p>
          </dl>
        </div>
      );
    return content;
  }
}
